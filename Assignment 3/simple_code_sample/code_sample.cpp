/**
* @file		main.cpp
* @date 	03/31/2019
* @author	Gabriel Ma�eru
* @par		Login: gabriel.m
* @par		Course: CS315
* @par		Assignment #3
* @brief 	Implementation of a main using the profiler
*/
#include "profiler.h"
#include "impl_profiler_output.h"

void do_some_random_overhead();
void multiple();
void recursive_calls(int i);

void A()
{
	PROFILE("A");
	for (unsigned i = 0; i < 10; i++)
		multiple();
}

void B()
{
	PROFILE("B");
	recursive_calls(5);
}

void C()
{
	PROFILE("C");
	A();
	B();
}

void main()
{
	for (unsigned i = 0; i < 10; i++)
	{
		START_FRAME("Main");
		A();
		B();
		C();
		END_FRAME();
	}
	output_analyzed_text();
}

void do_some_random_overhead()
{
	switch (rand() % 3)
	{
	case 0:
		// Do Nothing
		break;
	case 1:
		do_some_random_overhead();
		break;
	case 2:
		do_some_random_overhead();
		do_some_random_overhead();
		break;
	}
}

void multiple()
{
	PROFILE("Multiple Call");
	do_some_random_overhead();
}

void recursive_calls(int i)
{
	PROFILE("Recursive Call");
	do_some_random_overhead();
	if (i > 0)
		recursive_calls(i - 1);
}