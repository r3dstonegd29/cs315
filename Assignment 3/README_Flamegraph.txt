A Perl interpreter is required
	-If the perl interpreter is installed and attached to .pl extension, the program will automatically create the flamegraph result.
	-Otherwise the command should be executed manually:
		/>perl flamegraph.pl --width 1280 --height 25 --countname clockcycles --minwidth 0.01 <input file> > result.svg