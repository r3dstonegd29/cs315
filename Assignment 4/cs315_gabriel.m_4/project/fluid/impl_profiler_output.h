#pragma once
/*
	brief: output raw data
	frame_num:	- frame to output
				- "-1" for all frames
*/
void output_raw_text(int frame_num = -1);

/*
	brief: output a flamegraph
	frame_num:	- frame to output
				- "-1" for all frames
*/
void output_flamegraph();

/*
brief: analyse and output readable
*/
void output_analyzed_text();