/**
* @file		impl_profiler_output.cpp
* @date 	03/31/2019
* @author	Gabriel Mañeru
* @par		Login: gabriel.m
* @par		Course: CS315
* @par		Assignment #3
* @brief 	Implementation of a profiler
*/
#include "impl_profiler_output.h"
#include "profiler.h"
#include <functional>
#include <fstream>
#include <string>
#include <assert.h>
#include <Windows.h>
#include <unordered_map>

void output_raw_text(int frame_num)
{
	const std::vector<const profiler_node*>& p_frames = GET_PROFILER_DATA();
	assert(frame_num == -1 || frame_num < p_frames.size());
	std::ofstream file;
	auto pad = [](size_t size)->std::string
	{
		return std::string(size, ' ');
	};
	std::function<void(const profiler_node*, unsigned)> output_frame = [&output_frame, &file, &pad](const profiler_node* pNode, unsigned len)->void
	{
		if (pNode != nullptr)
		{
			file << pad(len) << pNode->m_title << ": " << pNode->m_time << "cc - " << pNode->m_calls << " call(s)";
			if (!pNode->m_childs.empty())
			{
				file << std::endl << pad(len) << "{" << std::endl;
				for (auto& c : pNode->m_childs)
					output_frame(c, len + 4);
				file << pad(len) << "}" << std::endl;
			}
			else
				file << std::endl;
		}
	};

	// Case Output All Frames
	if (frame_num == -1)
	{
		file.open("output-raw-all.txt");
		if (file.is_open())
		{
			for (size_t i = 0; i < p_frames.size(); i++)
			{
				file << "Frame #" << i << std::endl;
				output_frame(p_frames[i], 0);
				file << std::endl;
			}
			file.close();
		}
	}
	// Case Output Single Frame
	else
	{
		file.open("output-raw-#" + std::to_string(frame_num) + ".txt");
		if (file.is_open())
		{
			file << "Frame #" << frame_num << std::endl;
			output_frame(p_frames[frame_num], 0);
			file.close();
		}
	}
}

void output_flamegraph()
{
	struct LocalNode
	{
		time_stamp average_time;

		std::vector<std::tuple<time_stamp, unsigned, unsigned>> data;
		std::unordered_map<std::string, LocalNode*> childs;
		~LocalNode()
		{
			for (auto& c : childs)
				delete c.second;
		}
	};

	const std::vector<const profiler_node*>& p_frames = GET_PROFILER_DATA();
	LocalNode * root = new LocalNode{};

	// Merge all roots into node arrays
	for (int f = 0; f < p_frames.size(); f++)
	{
		std::function<void(LocalNode*, const profiler_node*)> insert = [&insert, &f](LocalNode* pNode, const profiler_node* pInNode)->void
		{
			if (pInNode == nullptr)
				return;

			pNode->data.push_back(std::make_tuple(pInNode->m_time, pInNode->m_calls, f));
			for (auto&c : pInNode->m_childs)
			{
				auto it = pNode->childs.find(c->m_title);
				if (it == pNode->childs.end())
				{
					pNode->childs[c->m_title] = new LocalNode{};
					it = pNode->childs.find(c->m_title);
				}
				insert((*it).second, c);
			}
		};
		insert(root, p_frames[f]);
	}

	// Analyse data into node arrays
	std::function<void(LocalNode*, time_stamp)> analyse = [&analyse](LocalNode* pNode, time_stamp total_time)->void
	{
		// Time computation
		for (auto& d : pNode->data)
			pNode->average_time += std::get<0>(d);

		// Average Time
		pNode->average_time /= static_cast<time_stamp>(pNode->data.size());

		for (auto& c : pNode->childs)
		{
			analyse(c.second, (total_time > 0) ? total_time : pNode->average_time);
		}
	};
	analyse(root, 0u);


	std::ofstream file;
	std::function<void(const LocalNode *, const std::string&)> output_frame = [&output_frame, &file](const LocalNode* pNode, const std::string& pre)->void
	{
		if (pNode != nullptr)
		{
			if (pNode->childs.size() == 0)
			{
				file << pre.substr(0, pre.find_last_of(';')) +' '+ std::to_string(pNode->average_time) << std::endl;
			}
			else
			{
				for (auto& c : pNode->childs)
					output_frame(c.second, pre + c.first + ";");
			}
		}
	};

	file.open("flamegraph-raw-all");
	if (file.is_open())
	{
		output_frame(root, "");
		file.close();
	}
}

void output_analyzed_text()
{
	struct LocalNode
	{
		time_stamp min_time{ ULONG_MAX }, max_time{ 0u };
		int frame_min, frame_max;
		time_stamp average_time;
		float p_average_time;
		int average_calls;

		std::vector<std::tuple<time_stamp, unsigned, unsigned>> data;
		std::unordered_map<std::string, LocalNode*> childs;
		~LocalNode()
		{
			for (auto& c : childs)
				delete c.second;
		}
	};

	const std::vector<const profiler_node*>& p_frames = GET_PROFILER_DATA();
	std::string title = p_frames[0]->m_title;
	LocalNode * root = new LocalNode{};

	// Merge all roots into node arrays
	for (int f =0; f < p_frames.size(); f++)
	{
		std::function<void(LocalNode*, const profiler_node*)> insert = [&insert, &f](LocalNode* pNode, const profiler_node* pInNode)->void
		{
			if (pInNode == nullptr)
				return;

			pNode->data.push_back(std::make_tuple(pInNode->m_time, pInNode->m_calls, f));
			for (auto&c : pInNode->m_childs)
			{
				auto it = pNode->childs.find(c->m_title);
				if (it == pNode->childs.end())
				{
					pNode->childs[c->m_title] = new LocalNode{};
					it = pNode->childs.find(c->m_title);
				}
				insert((*it).second, c);
			}
		};
		insert(root, p_frames[f]);
	}

	// Analyse data into node arrays
	std::function<void(LocalNode*, time_stamp)> analyse = [&analyse](LocalNode* pNode, time_stamp total_time)->void
	{
		// Time computation
		for (auto& d : pNode->data)
		{
			time_stamp t = std::get<0>(d);
			if (t < pNode->min_time) pNode->min_time = t, pNode->frame_min = std::get<2>(d);
			if (t > pNode->max_time) pNode->max_time = t, pNode->frame_max = std::get<2>(d);
			pNode->average_time += t;
		}

		// Average Time
		pNode->average_time /= static_cast<time_stamp>(pNode->data.size());

		// Average Percentage
		if (total_time > 0)
			pNode->p_average_time = static_cast<float>(static_cast<double>(pNode->average_time) / static_cast<double>(total_time));
		else
			pNode->p_average_time = 1.0f;

		// Calls computation
		for (auto& d : pNode->data)
			pNode->average_calls += std::get<1>(d);

		// Average Calls
		pNode->average_calls /= static_cast<int>(pNode->data.size());

		for (auto& c : pNode->childs)
		{
			analyse(c.second, (total_time > 0)? total_time : pNode->average_time);
		}
	};
	analyse(root, 0u);


	// Output final gathering into file
	std::ofstream file;
	auto pad = [](size_t size)->std::string
	{
		return std::string(size, ' ');
	};
	std::function<void(const LocalNode*, unsigned)> output_frame = [&output_frame, &file, &pad](const LocalNode* pNode, unsigned len)->void
	{
		if (pNode != nullptr)
		{
			file << pad(len)
				<< "Average: " << pNode->average_time << "cc - "
				<< std::to_string(100.f * pNode->p_average_time) << "% - "
				<< pNode->average_calls << " calls" << std::endl;
			file << pad(len)
				<< "Min: " << pNode->min_time << "cc[#" << pNode->frame_min << "] - "
				<< "Max: " << pNode->max_time << "cc[#" << pNode->frame_max << "]";
			if (!pNode->childs.empty())
			{
				file << std::endl << pad(len) << "{" << std::endl;
				bool off = false;
				for (auto& c : pNode->childs)
				{
					if (off) file << std::endl;
					file << pad(len + 4) << c.first << std::endl;
					output_frame(c.second, len + 4);
					off = true;
				}
				file << pad(len) << "}" << std::endl;
			}
			else
				file << std::endl;
		}
	};
	file.open("output-analyzed.txt");
	if (file.is_open())
	{
		file << title << std::endl;
		output_frame(root, 0);
		file.close();
	}
}