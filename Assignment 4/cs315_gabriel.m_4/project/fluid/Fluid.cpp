#include "Fluid.h"
#include "profiler.h"
#include "impl_profiler_output.h"
#include <xmmintrin.h>
#include <list>

// Zero the fluid simulation member variables for sanity
Fluid::Fluid() 
{
	step = 0;
	paused = false;
	pause_step = 0xFFFFFFFF;

	width = 0;
	height = 0;
	grid_w = 0;
	grid_h = 0;

	num_particles = 0;
	pos = NULL;
	vel = NULL;
	acc = NULL;
	density = NULL;
	pressure = NULL;
	grid = NULL;
	num_neighbors = 0;
	// If this value is too small, ExpandNeighbors will fix it
	neighbors_capacity = 265 * 1200;
	neighbors = new FluidNeighborRecord[ neighbors_capacity ];

	// Precompute kernel coefficients
	// See "Particle-Based Fluid Simulation for Interactive Applications"
	// "Poly 6" Kernel - Used for Density
	poly6_coef = 315.0f / (64.0f * glm::pi<float>() * glm::pow(FluidSmoothLen, 9));
	// Gradient of the "Spikey" Kernel - Used for Pressure
	grad_spiky_coef = -45.0f / (glm::pi<float>() * glm::pow(FluidSmoothLen, 6));
	// Laplacian of the "Viscosity" Kernel - Used for Viscosity
	lap_vis_coef = 45.0f / (glm::pi<float>() * glm::pow(FluidSmoothLen, 6));
}

// Destructor
Fluid::~Fluid() 
{
	Clear();
	num_neighbors = 0;
	neighbors_capacity = 0;
	delete[] neighbors; neighbors = neighbors;
	output_analyzed_text();
	output_raw_text();
	output_flamegraph();
}

// Create the fluid simulation
// width/height is the simulation world maximum size
void Fluid::Create(float w, float h) 
{
	width = w;
	height = h;
	grid_w = (int)(width / FluidSmoothLen);
	grid_h = (int)(height / FluidSmoothLen);

	delete[] grid;
	grid = new Cell[ grid_w * grid_h ];
}



// Fill a region in the lower left with evenly spaced particles
void Fluid::Fill(float size) 
{
	Clear();

	int w = (int)(size / FluidInitialSpacing);
	// Allocate
	num_particles = w * w;
	pos = new vec2[num_particles];
	vel = new vec2[num_particles];
	acc = new vec2[num_particles];
	density = new float[num_particles];
	pressure = new float[num_particles];

	// Populate
	for ( int x = 0 ; x < w ; x++ )
	{
		for ( int y = 0 ; y < w ; y++ )	 
		{
			pos[ y*w+x ] = vec2(x * FluidInitialSpacing, Height() - y * FluidInitialSpacing);
			vel[ y*w+x ] = vec2(0, 0);
			acc[ y*w+x ] = vec2(0, 0);
			density[ y*w+x ] = 0;
			pressure[ y*w+x ] = 0;
		}
	}
}

// Remove all particles
void Fluid::Clear() 
{
	step = 0;
	num_particles = 0;
	delete[] pos; pos = NULL;
	delete[] vel; vel = NULL;
	delete[] acc; acc = NULL;
	delete[] density; density = NULL;
	delete[] pressure; pressure = NULL;
}

// Expand the Neighbors list if necessary
// This function is rarely called
__forceinline void Fluid::ExpandNeighbors() 
{
	// Double the size of the neighbors array because it is full
	neighbors_capacity *= 2;
	FluidNeighborRecord* new_neighbors = new FluidNeighborRecord[ neighbors_capacity ];
	memcpy( new_neighbors, neighbors, sizeof(FluidNeighborRecord) * num_neighbors );
	delete[] neighbors;
	neighbors = new_neighbors;
}

// Add Two new neighbors if they are close enought
__forceinline void Fluid::AddNeighbor(unsigned A, unsigned B)
{
	// Search radius is the smoothing length
	const float h2 = FluidSmoothLen*FluidSmoothLen;
	vec2 d = pos[A] - pos[B];
	float distsq = d.x * d.x + d.y * d.y;

	// Check that the particle is within the smoothing length
	if (distsq < h2)
	{
		if (num_neighbors >= neighbors_capacity)
		{
			ExpandNeighbors();
		}

		// Record the ID of the two particles
		// And record the squared distance
		FluidNeighborRecord& record = neighbors[num_neighbors];
		record.p = A;
		record.n = B;
		record.distsq = distsq;
		num_neighbors++;
	}
}

// Add Permutation of two vectors as neighbors
__forceinline void Fluid::CheckNeighborCell(const Cell & current_cell, const unsigned int * current, unsigned pos)
{
	const Cell& other_cell = grid[pos];
	if (other_cell.used > 0)
	{
		const unsigned int * other = &other_cell.m_indices.front();
		for (unsigned i = 0; i < current_cell.used; i++)
			for (unsigned j = 0; j < other_cell.used; j++)
				AddNeighbor(current[i], other[j]);
	}
}

// Simulation Update
// Build the grid of neighbors
// Imagine an evenly space grid.  All of our neighbors will be
// in our cell and the 8 adjacent cells
void Fluid::UpdateGrid() 
{
	PROFILE("Update Grid");
	// Cell size is the smoothing length

	// Clear the offsets
	for( int cell = 0; cell < (grid_w * grid_h); cell++ )
		grid[cell].used = 0;

	// Count the number of particles in each cell
	for( unsigned int particle = 0; particle < num_particles; particle++ ) 
	{
		// Find where this particle is in the grid
		int p_gx = glm::min(glm::max((int)(pos[particle].x * (1.0 / FluidSmoothLen)), 0), grid_w - 1);
		int p_gy = glm::min(glm::max((int)(pos[particle].y * (1.0 / FluidSmoothLen)), 0), grid_h - 1);
		int cell = p_gy * grid_w + p_gx ;
		grid[ cell ].add(particle);
	}
}

// Simulation Update
// Build a list of neighbors (particles from adjacent grid locations) for every particle
void Fluid::GetNeighbors() 
{
	PROFILE("Get Neighbors");
	num_neighbors = 0;

	const int cell_count = grid_h * grid_w;
	for (int cell = 0; cell < cell_count; cell++)
	{
		const Cell& current_cell = grid[cell];
		if (current_cell.used > 0)
		{
			// Get Current Head pointer
			const unsigned int * start = &current_cell.m_indices.front();

			// Check against Current Cell
			for (unsigned i = 0; i < current_cell.used - 1; i++)
				for (unsigned j = i + 1; j < current_cell.used; j++)
					AddNeighbor(start[i], start[j]);

			// Check against Neighbors
			// Compute Boundaries
			const int w_ = cell % grid_w;
			bool available_west = w_ > 0;
			bool available_east = w_ < grid_w - 1;
			bool available_south = cell + grid_w < cell_count;

			// Check East
			if (available_east)
				CheckNeighborCell(current_cell, start, cell + 1);

			// Check South Cells
			if (available_south)
			{
				// Check South-West
				if (available_west)
					CheckNeighborCell(current_cell, start, cell + grid_w - 1);

				// Check South
				CheckNeighborCell(current_cell, start, cell + grid_w);

				// Check South-East
				if (available_east)
					CheckNeighborCell(current_cell, start, cell + grid_w + 1);
			}
		}
	}
}

// Simulation Update
// Compute the density for each particle based on its neighbors within the smoothing length
void Fluid::ComputeDensity() 
{
	PROFILE("Compute Density");
	const float base_density = glm::pow(FluidSmoothLen, 6) * FluidWaterMass;
	#pragma omp parallel for num_threads(3)
	for( int particle = 0; particle < num_particles; particle++ ) 
	{
		// This is r = 0
		density[particle] = base_density;
	}

	// Extract needed precomputations
	const int remain = num_neighbors % 4;
	const int optimization_limit = num_neighbors - remain;
	const __m128 fluid_smooth_len_2 = _mm_set1_ps(FluidSmoothLen * FluidSmoothLen);
	const __m128 fluid_water_mass = _mm_set1_ps(FluidWaterMass);

	// foreach neighboring pair of particles
	#pragma omp parallel for num_threads(3)
	for (int i = 0; i < optimization_limit; i += 4)
	{
		// Load Distance^2
		const __m128 dist_sq = _mm_set_ps(neighbors[i].distsq, neighbors[i + 1].distsq, neighbors[i + 2].distsq, neighbors[i + 3].distsq);
			
		// Compute Density
		{
			// Compute
			const __m128 h_r = _mm_sub_ps(fluid_smooth_len_2, dist_sq);
			const __m128 inv_volume = _mm_mul_ps(_mm_mul_ps(h_r, h_r), h_r);
			__m128 dens = _mm_mul_ps(inv_volume, fluid_water_mass);

			// Store
			float res[4];
			_mm_store_ps(res, dens);
			density[neighbors[i].p] += res[3];
			density[neighbors[i].n] += res[3];
			density[neighbors[i+1].p] += res[2];
			density[neighbors[i+1].n] += res[2];
			density[neighbors[i+2].p] += res[1];
			density[neighbors[i+2].n] += res[1];
			density[neighbors[i+3].p] += res[0];
			density[neighbors[i+3].n] += res[0];
		}
		// Compute Square Root
		{
			// Compute
			__m128 real_sqrt = _mm_sqrt_ps(dist_sq);

			// Store
			float res[4];
			_mm_store_ps(res, real_sqrt);
			neighbors[i].distsq = res[3];
			neighbors[i + 1].distsq = res[2];
			neighbors[i + 2].distsq = res[1];
			neighbors[i + 3].distsq = res[0];
		}
	}

	// Copmute remaining neighbors without parallelisation
	for (int i = optimization_limit; i < num_neighbors; i++)
	{
		float dens = FluidWaterMass * glm::pow(FluidSmoothLen * FluidSmoothLen - neighbors[i].distsq, 3);
		density[neighbors[i].p] += dens;
		density[neighbors[i].n] += dens;
		neighbors[i].distsq = glm::sqrt(neighbors[i].distsq);
	}

	// Approximate pressure as an ideal compressible gas
	// based on a spring eqation relating the rest density
	#pragma omp parallel for num_threads(3)
	for( int particle = 0 ; particle < num_particles ; ++particle ) 
	{
		density[particle] *= poly6_coef;
		pressure[particle] = FluidStiff * glm::max(pow(density[particle] / FluidRestDensity, 3) - 1, 0.0f);
	}
}

// Simulation Update
// Compute the forces based on the Navier-Stokes equations for laminer fluid flow
// Follows is lots more voodoo
void Fluid::ComputeForce() 
{
	PROFILE("Compute Force");

	// foreach neighboring pair of particles
	#pragma omp parallel for num_threads(3)
	for(int i = 0; i < static_cast<int>(num_neighbors); i++ ) 
	{				
		// Compute force due to pressure and viscosity
		float h_r = FluidSmoothLen - neighbors[i].distsq;
		vec2 diff = pos[neighbors[i].n] - pos[neighbors[i].p];

		// Forces is dependant upon the average pressure and the inverse distance
		// Force due to pressure is:
		// 1/rho_p * 1/rho_n * Pavg * W(h, r)
		// Where the smoothing kernel is:
		// The gradient of the "Spikey" kernel
		vec2 force = (0.5f * (pressure[neighbors[i].p]+pressure[neighbors[i].n]) * grad_spiky_coef * h_r / neighbors[i].distsq ) * diff;
		
		// Viscosity is based on relative velocity
		// Viscosity is:
		// 1/rho_p * 1/rho_n * Vrel * mu * W(h, r)
		// Where the smoothing kernel is:
		// The laplacian of the "Viscosity" kernel
		force += ( (FluidViscosity * lap_vis_coef) * (vel[neighbors[i].n] - vel[neighbors[i].p]) );
		
		// Throw in the common (h-r) * 1/rho_p * 1/rho_n
		force *= h_r * FluidWaterMass / (density[neighbors[i].p] * density[neighbors[i].n]);
		
		// Apply force - equal and opposite to both particles
		// Critical Zone:	
		acc[neighbors[i].p] += force;
		acc[neighbors[i].n] -= force;
	}
}

// Simulation Update
// Integration
void Fluid::Integrate( float dt ) 
{
	PROFILE("Integrate");

	#pragma omp parallel for num_threads(3)
	for( int particle = 0 ; particle < static_cast<int>(num_particles) ; ++particle ) 
	{
		// Collision Boundaries
		acc[particle].x += glm::min(pos[particle].x, 0.0f) * -FluidStaticStiff;
		acc[particle].y += glm::min(pos[particle].y, 0.0f) * -FluidStaticStiff;
		acc[particle].x -= glm::min(-pos[particle].x + width, 0.0f) * -FluidStaticStiff;
		acc[particle].y -= glm::min(-pos[particle].y + height, 0.0f) * -FluidStaticStiff;

		// Acceleration
		acc[particle].y += 1.0f;

		// Integration - Euler-Cromer
		vel[particle] += dt * acc[particle];
		pos[particle] += dt * vel[particle];
		acc[particle] = vec2(0, 0);
	}
}

// Simulation Update
void Fluid::Update( float dt ) 
{
	START_FRAME("Fluid");
	// Pause runs the simulation standing still for profiling
	if( paused || step == pause_step ) { dt = 0.0f; }
	else { step++; }

	// Create neighbor information
	UpdateGrid();
	GetNeighbors();

	// Calculate the forces for all of the particles
	ComputeDensity();
	ComputeForce();

	// And integrate
	Integrate(dt);
	END_FRAME();
}
