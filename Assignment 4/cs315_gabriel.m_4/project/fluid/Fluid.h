
#pragma once
#include <vector>
#include "glm\glm.h"

// Fluid magic numbers
const float FluidTimestep = 0.005f;
const float FluidSmoothLen = 0.012f;
const float FluidStaticStiff = 3000.0f;
const float FluidRestDensity = 1000.0f;
const float FluidWaterMass = 0.0002f;
const float FluidViscosity = 0.1f;
const float FluidStiff = 200.0f;
const float FluidInitialSpacing = 0.0045f;

/*****************************************************************************/

struct FluidNeighborRecord 
{
	unsigned int p; // Particle Index
	unsigned int n; // Neighbor Index		
	float distsq; // Distance Squared and Non-Squared (After Density Calc)
};

struct Cell
{
	std::vector<unsigned int> m_indices;
	size_t used{ 0 };
	void add(unsigned particle)
	{
		if (used == m_indices.size())
		{
			if (used == 0)
				m_indices.resize(2);
			else
				m_indices.resize(m_indices.size() * 2);
		}
		m_indices[used++] = particle;
	}
};

/*****************************************************************************/


class Fluid 
{
	public:
		/* Common Interface */
		Fluid();
		~Fluid();

		void Create(float w, float h);
		void Fill(float size);
		void Clear();
		void Update(float dt);

		/* Common Data */
		unsigned int num_particles;
		vec2 * pos;
		vec2 * vel;
		vec2 * acc;
		float * density;
		float * pressure;
		Cell * grid;
		unsigned int neighbors_capacity;
		unsigned int num_neighbors;
		FluidNeighborRecord * neighbors;
		
		unsigned int Size()					{ return num_particles; }
		unsigned int Step()					{ return step; }
		void Pause( bool p )				{ paused = p; }
		void PauseOnStep( unsigned int p )	{ pause_step = p; }
		float Width()						{ return width; }
		float Height()						{ return height; }
		

	private:
		/* Simulation */
		void UpdateGrid();
		__forceinline void ExpandNeighbors();
		__forceinline void AddNeighbor(unsigned A, unsigned B);
		__forceinline void CheckNeighborCell(const Cell& current_cell, const unsigned int * current, unsigned pos);
		void GetNeighbors();
		void ComputeDensity();
		void ComputeForce();
		void Integrate(float dt);

	private:
		/* Run State */
		unsigned int step;
		bool paused;
		unsigned int pause_step;

		/* World Size */
		float width;
		float height;
		int grid_w;
		int grid_h;

		/* Coefficients for kernel */
		float poly6_coef;
		float grad_spiky_coef;
		float lap_vis_coef;
};
