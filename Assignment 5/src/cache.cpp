#include "cache.h"

CacheData::CacheData(Memory<4u>& ram_, CacheType type_, LineEvictionPolicy policy_)
	: policy(policy_), ram(&ram_)
{
	// Initizalize Cache properties
	switch (type_)
	{
	case CacheData::e_DirectMapped:
		associativity = 1u;
		set_count = 4u;
		block_size = 2u;
		size = 8u;
		break;
	case CacheData::e_SetAssociative:
		associativity = 2u;
		set_count = 2u;
		block_size = 2u;
		size = 8u;
		break;
		break;
	case CacheData::e_FullyAssociative:
		associativity = 4u;
		set_count = 1u;
		block_size = 2u;
		size = 8u;
		break;
	}

	// Populate sets
	CacheLine line;
	line.blocks.resize(block_size);
	line.valid = false;
	CacheSet set;
	set.lines.resize(associativity, line);
	sets.resize(set_count, set);
}

void CacheData::instruction_entry(size_t add, Statistics & stat)
{
	// Find Set
	size_t set_num = get_set(add);
	CacheSet& set = sets[set_num];

	// Find Line Conflict
	size_t line_num = get_line(add, set);
	if (line_num != UNCACHED_LINE) // No Conflict
	{

		// Line is empty
		if (!set.lines[line_num].valid) // Cold Miss
		{
			// update stats
			stat[Statistics::cold_misses]++;
			stat[Statistics::cache_misses]++;

			// update cache line
			CacheLine& line = set.lines[line_num];
			line.valid = true;
			line.tag = get_tag(add);
			load_memory(add, line.blocks.data());

			// Update set heuristics
			set.frequency[line_num] = 1;
			set.last_used.push_back(line_num);
		}

		// Data already cached
		else // Cache Hit
		{
			// update stats
			stat[Statistics::cache_hits]++;

			// Update set heuristics
			set.frequency[line_num]++;
			set.last_used.erase(std::find(set.last_used.begin(), set.last_used.end(), line_num));
			set.last_used.push_back(line_num);
		}
	}
	else // Cache Miss
	{
		// update stats
		stat[Statistics::conflict_misses]++;
		stat[Statistics::cache_misses]++;

		// find new line using heuristics policy
		switch (policy)
		{
		case CacheData::e_none:
			line_num = 0;
			break;
		case CacheData::e_Random:
			line_num = rand() % associativity;
			break;
		case CacheData::e_LRU:
			line_num = set.last_used.front();
			break;
		case CacheData::e_LFU:
			{
				size_t min = UINT_MAX;
				for (auto& f : set.frequency)
					if (f.second < min)
						min = f.second, line_num = f.first;
			}
			break;
		}

		// update cache line
		CacheLine& line = set.lines[line_num];
		line.tag = get_tag(add);
		load_memory(add, line.blocks.data());

		// Update set heuristics
		set.frequency[line_num] = 1;
		set.last_used.erase(std::find(set.last_used.begin(), set.last_used.end(), line_num));
		set.last_used.push_back(line_num);
	}
}

bool CacheData::is_address_cached(size_t add)
{
	// Find Set
	size_t set_num = get_set(add);
	CacheSet& set = sets[set_num];

	// Find Line
	return get_line(add, set) != UNCACHED_LINE;
}

size_t CacheData::get_set(size_t add)
{
	// Remove Block Bit
	add >>= block_size >> 1;

	// mask for set bits
	return add & (1 << (set_count >> 1)) - 1;
}

size_t CacheData::get_line(size_t add, const CacheSet & set)
{
	// Find Same Tag Lines
	for (size_t i = 0; i < associativity; i++)
		if (set.lines[i].valid && set.lines[i].tag == get_tag(add))
			return i;

	// Find Empty Lines
	for (size_t i = 0; i < associativity; i++)
		if (!set.lines[i].valid)
			return i;

	// Line is Conflict
	return UNCACHED_LINE;
}

size_t CacheData::get_tag(size_t add)
{
	// Remove Block Bit
	add >>= (block_size >> 1) + (set_count >> 1);

	// mask for tag bits
	return add & ((1 << ((associativity >> 1) + 1)) - 1);
}

void CacheData::load_memory(size_t add, unsigned char * block_head)
{
	// Offset address
	size_t head_address = add - (add % block_size);
	
	// copy data
	for (size_t i = 0; i < block_size; i++)
		block_head[i] = (*ram)[head_address + i];
}
