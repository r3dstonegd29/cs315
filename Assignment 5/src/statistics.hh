#ifndef _STATISTICS_H_
#define _STATISTICS_H_

#include <cstddef> // std::size_t
#include <array>

// collection of statistics gathered during a memory trace run
// very simple implementation, consider wrapping something more sophisticated around this
struct Statistics
{
    inline unsigned int & operator [] ( std::size_t e )
    {
        return counters[e];
    }
    inline unsigned int operator [] ( std::size_t e ) const
    {
        return counters[e];
    }
    
    enum // named indices for the counter array
    {
        instruction_references, data_references, store_instructions, load_instructions,
        cache_hits, cache_misses, cold_misses, conflict_misses
    };

private:
	std::array<unsigned int, 8>counters{};

}; // Statistics

#endif

