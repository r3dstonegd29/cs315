#pragma once
#include "include\gtest\gtest.h"
#include "cpu.h"

class iTest : public ::testing::Test
{
	Memory<4u> ram_memory;
	CacheData cache;

public:
	CpuData cpu;

	iTest(CacheData::CacheType type, CacheData::LineEvictionPolicy policy = CacheData::e_none)
		: cache(ram_memory, type, policy), cpu(cache) {}
};

class CacheMechanics : public iTest
{
public:
	CacheMechanics()
		: iTest(CacheData::e_DirectMapped) {}
};

class DirectMappedCacheTest : public iTest
{
public:
	DirectMappedCacheTest()
		: iTest(CacheData::e_DirectMapped) {}
};

class SetAssociativeCacheTest : public iTest
{
public:
	SetAssociativeCacheTest()
		: iTest(CacheData::e_SetAssociative, CacheData::e_Random) {}
};

class FullyAssociativeCacheTest : public iTest
{
public:
	FullyAssociativeCacheTest()
		: iTest(CacheData::e_FullyAssociative, CacheData::e_Random) {}
};

class LineEvictionPolicyTest : public iTest
{
public:
	LineEvictionPolicyTest()
		: iTest(CacheData::e_FullyAssociative) {}
};

class RandomLineEvictionPolicyTest : public iTest
{
public:
	RandomLineEvictionPolicyTest()
		: iTest(CacheData::e_FullyAssociative, CacheData::e_Random) {}
};

class LRULineEvictionPolicyTest : public iTest
{
public:
	LRULineEvictionPolicyTest()
		: iTest(CacheData::e_FullyAssociative, CacheData::e_LRU) {}
};

class LFULineEvictionPolicyTest : public iTest
{
public:
	LFULineEvictionPolicyTest()
		: iTest(CacheData::e_FullyAssociative, CacheData::e_LFU) {}
};