#ifndef _MEMORY_HH_
#define _MEMORY_HH_

#include <array>    // std::array


// representation of the main memory module
// it's just an array of bytes
// the size of the memory is defined by the address space
// i.e.: 8-bit address space = 2^8 = 256 addressable bytes
template <std::size_t AddressSpace> // size (in bits) of the address space
class Memory
{
public:
    static const std::size_t addressable_bytes = 1 << AddressSpace;

    Memory()
    {   // fill data with alphanumeric characters to make memory contents distinguishable
        for( std::size_t i = 0; i < addressable_bytes; ++i ) 
            data[i] = (i % 62) + 65;
    }

    inline       unsigned char & operator [] ( const std::size_t i )       { return data[i]; }
    inline const unsigned char & operator [] ( const std::size_t i ) const { return data[i]; }


private:
    std::array<unsigned char, addressable_bytes> data;

}; // Memory

#endif
