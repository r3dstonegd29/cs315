#pragma once
#include "trace.hh"
#include "statistics.hh"
#include "cache.h"
struct CpuData
{
	// Constructor
	CpuData(CacheData& cache_)
		: cache(&cache_) {}

	// Bus
	CacheData* cache;

	//Public Interface
	Statistics run(const MemoryTrace& t);
	CacheData get_cache(size_t pos);
};