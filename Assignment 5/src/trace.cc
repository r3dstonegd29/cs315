#include "trace.hh"

#include <cstdlib> // std::strtoul
#include <fstream> // std::ifstream
#include <stdexcept> // std::invalid_argument

// MemoryTrace implementation -------------------------------------------------------------------------------

// construct a memory trace instance out of a file that contains trace information
MemoryTrace::MemoryTrace( const std::string & filename )
    : entries( load_from_file(filename) ) 
{}

// construct a memory trace instance out of a sequence of in-memory strings
MemoryTrace::MemoryTrace( const std::vector<std::string> & lines )
    : entries( load_from_strings(lines) ) 
{}

MemoryTrace::Entry MemoryTrace::parse_trace_line( const std::string & line )
{
    auto type_pos  = line.find_first_not_of(' ');
    auto comma_pos = line.find(',');
    // extract the substrings for the data tokens given the separator positions
    auto addr_str = line.substr( 3, comma_pos ); // address always start at position 3; from there, grab 8 chars
    auto size_str = line.substr( comma_pos + 1, 1 ); // grab 1 char after the comma

    // extract the actual data out of the data substring
    char * end;
    char type = *line.substr( type_pos, 1 ).c_str(); // grab the first char that is not a blankspace
    std::size_t addr = std::strtoul( addr_str.c_str(), &end, 16 ); 
    std::size_t size = std::strtoul( size_str.c_str(), &end, 10 ); 

    // return the 3-tuple as a memory trace entry
    return { type, addr, size };
};

// load a file that contains a memory trace and read its contents into a memory trace in-memory representation
std::vector<MemoryTrace::Entry> MemoryTrace::load_from_file( const std::string & filename )
{
    // open the requested file
    std::ifstream trace_file( filename );
    if( !trace_file.good() ) throw std::invalid_argument("can't open file " + filename);

    std::vector<Entry> result;
    std::string line;
    // read the whole file, one line at a time
    while( std::getline( trace_file, line ) )
    {   // store the result of parsing the current line into the result container
        result.emplace_back( parse_trace_line(line) );
    }

    return result;
}

// load a memory trace from a sequence of in-memory strings
// this is useful for running small tests/scenarios
std::vector<MemoryTrace::Entry> MemoryTrace::load_from_strings( const std::vector<std::string> & strings )
{
    std::vector<Entry> result;
    // store the result of parsing each string into the result container
    for( const std::string & line : strings )
    {   
        result.emplace_back( parse_trace_line(line) );
    }
    return result;
}

const MemoryTrace::Entry & MemoryTrace::operator [] ( const std::size_t i ) const
{
    return entries[i];
}

// MemoryTrace::Entry implementation ------------------------------------------------------------------------

MemoryTrace::Entry::Entry( char t, std::size_t a, std::size_t s )
    : type{static_cast<event_type>(t)}, address{a}, size{s} 
{}

