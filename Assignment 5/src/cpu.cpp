#include "cpu.h"

Statistics CpuData::run(const MemoryTrace & t)
{
	Statistics stats;
	for (auto it = t.begin(); it != t.end(); it++)
	{
		auto entry = *it;
		switch (entry.type)
		{
		case MemoryTrace::Entry::event_type::instruction:
			cache->instruction_entry(entry.address, stats);
			stats[Statistics::instruction_references]++;
			break;
		case MemoryTrace::Entry::event_type::memory_load:
			// Behaviour Not Specify
			stats[Statistics::load_instructions]++;
			stats[Statistics::data_references]++;
			break;
		case MemoryTrace::Entry::event_type::memory_store:
			// Behaviour Not Specify
			stats[Statistics::store_instructions]++;
			stats[Statistics::data_references]++;
			break;
		}
	}
	return stats;
}

CacheData CpuData::get_cache(size_t)
{
	return *cache;
}
