#ifndef _MEMORY_TRACE_
#define _MEMORY_TRACE_

#include <vector> // std::vector
#include <string> // std::string

// a memory trace is a sequence of memory reference requests issued by a particular (sub)program
class MemoryTrace
{
public:
    // construct a memory trace instance out of a file that contains trace information
    MemoryTrace( const std::string & filename );
    // construct a memory trace instance out of a sequence of in-memory strings
    MemoryTrace( const std::vector<std::string> & lines );

    // each entry in the memory trace is defined by a 3-tuple:
    //   - access type: instruction read, memory store or memory load
    //   - address: the physical/virtual (the difference here is irrelevant) memory address being referenced
    //   - size: amount of memory being referenced
    struct Entry
    {
        Entry( char t, std::size_t a, std::size_t s );

        enum class event_type : char 
        { 
            instruction  = 'I',
            memory_store = 'S',
            memory_load  = 'L' 
        };

        event_type     type;
        std::size_t address;
        std::size_t    size;
    
    }; // Entry

    const Entry & operator [] ( const std::size_t i ) const;

    // to iterate and use std algorithms
    inline std::vector<Entry>::const_iterator begin() const { return entries.cbegin(); }
    inline std::vector<Entry>::const_iterator end() const   { return entries.cend(); }

private:

    // parse a single string-formatted line into a memory trace entry
    // a string formatted entry has the layout: "<type> <address>,<size>"
    static Entry parse_trace_line( const std::string & line );

    // load a file that contains a memory trace and read its contents into a memory trace in-memory representation
    static std::vector<Entry> load_from_file( const std::string & filename );

    // load a memory trace from a sequence of in-memory strings
    // this is useful for running small tests/scenarios
    static std::vector<Entry> load_from_strings( const std::vector<std::string> & strings );

    // the sequence of entries that configure the memory trace 
    const std::vector<Entry> entries;

}; // MemoryTrace

#endif

