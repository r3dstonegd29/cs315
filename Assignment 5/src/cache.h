#pragma once
#include "memory.hh"
#include "statistics.hh"
#include <vector>
#include <map>

struct CacheData
{
	// Data Structures
	struct CacheLine
	{
		bool valid;
		unsigned char tag;
		std::vector<unsigned char> blocks;
	};
	struct CacheSet
	{
		std::vector<CacheLine> lines;
		std::vector<size_t> last_used;
		std::map<size_t, size_t> frequency;
	};
	
	// Enums
	enum CacheType { e_DirectMapped, e_SetAssociative, e_FullyAssociative };
	enum LineEvictionPolicy { e_none, e_Random, e_LRU, e_LFU };
	
	// Public Interface
	CacheData(Memory<4u>& ram_, CacheType type_, LineEvictionPolicy policy_);
	void instruction_entry(size_t add, Statistics& stat = Statistics{});
	bool is_address_cached(size_t add);
	
	// Properties
	size_t associativity;
	size_t set_count;
	size_t block_size;
	size_t size;
	LineEvictionPolicy policy;

private:
	// Bus
	Memory<4u>* ram;

	// Data
	std::vector<CacheSet> sets;

	// Utilities
	size_t get_set(size_t add);
	size_t get_line(size_t add, const CacheSet& set);
	size_t get_tag(size_t add);
	void load_memory(size_t add, unsigned char * block_head);
};
#define UNCACHED_LINE UINT_MAX