/***********************************
Name:       Compute Edge using Sobel
Author:     gabriel.m
Class:      cs315
Assignment: 2
***********************************/
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "bmp.h"
#include "measure.h"
#include <iostream>

/* just used for time measurements */
#define REP 10

typedef void (*test_func) (unsigned char *, unsigned char *, unsigned,
                           unsigned);

void compute_sobel(unsigned char *data_out,
	unsigned char *data_in,
	const unsigned& height,
	const unsigned& width,
	const int& lay, const int& i, const int& j)
{
	static const __m128i zero = _mm_setzero_si128();
	static const unsigned size = { height * width };
	static __m128i sumh{}, sumv{};
	static __m128i sobel[2][8]{};
	static __m128i packed[8]{};
	static __m128i total[2]{};
	static __m128i total_packed{};

	// load 8 arrays corresponding to the 16 versions of the 8 neighbours 
	packed[0] = _mm_loadu_si128(reinterpret_cast<__m128i*>(data_in + lay*size + (i - 1)*width + (j - 1)));
	packed[1] = _mm_loadu_si128(reinterpret_cast<__m128i*>(data_in + lay*size + (i)*width + (j - 1)));
	packed[2] = _mm_loadu_si128(reinterpret_cast<__m128i*>(data_in + lay*size + (i + 1)*width + (j - 1)));
	packed[3] = _mm_loadu_si128(reinterpret_cast<__m128i*>(data_in + lay*size + (i - 1)*width + (j)));
	packed[4] = _mm_loadu_si128(reinterpret_cast<__m128i*>(data_in + lay*size + (i + 1)*width + (j)));
	packed[5] = _mm_loadu_si128(reinterpret_cast<__m128i*>(data_in + lay*size + (i - 1)*width + (j + 1)));
	packed[6] = _mm_loadu_si128(reinterpret_cast<__m128i*>(data_in + lay*size + (i)*width + (j + 1)));
	packed[7] = _mm_loadu_si128(reinterpret_cast<__m128i*>(data_in + lay*size + (i + 1)*width + (j + 1)));

	// Unpack into two shorts
	for (int n = 0; n < 8; n++)
	{
		sobel[0][n] = _mm_unpacklo_epi8(packed[n], zero);
		sobel[1][n] = _mm_unpackhi_epi8(packed[n], zero);
	}

	// Compute Sobel
	for (int n = 0; n < 2; n++)
	{
		// Add following Sobel Matrices
		// Add N-W
		sumv = _mm_sub_epi16(zero, sobel[n][0]);
		sumh = _mm_sub_epi16(zero, sobel[n][0]);

		// Add N
		sumv = _mm_sub_epi16(sumv, sobel[n][1]);
		sumv = _mm_sub_epi16(sumv, sobel[n][1]);

		// Add N-E
		sumv = _mm_sub_epi16(sumv, sobel[n][2]);
		sumh = _mm_add_epi16(sumh, sobel[n][2]);

		// Add W
		sumh = _mm_sub_epi16(sumh, sobel[n][3]);
		sumh = _mm_sub_epi16(sumh, sobel[n][3]);
		
		// Add E
		sumh = _mm_add_epi16(sumh, sobel[n][4]);
		sumh = _mm_add_epi16(sumh, sobel[n][4]);

		// Add S-W
		sumv = _mm_add_epi16(sumv, sobel[n][5]);
		sumh = _mm_sub_epi16(sumh, sobel[n][5]);

		// Add S
		sumv = _mm_add_epi16(sumv, sobel[n][6]);
		sumv = _mm_add_epi16(sumv, sobel[n][6]);

		// Add S-E
		sumv = _mm_add_epi16(sumv, sobel[n][7]);
		sumh = _mm_add_epi16(sumh, sobel[n][7]);

		// Compute Absolute Value
		sumh = _mm_abs_epi16(sumh);
		sumv = _mm_abs_epi16(sumv);

		// Divide by 8
		sumh = _mm_srli_epi16(sumh, 3);
		sumv = _mm_srli_epi16(sumv, 3);

		// Add Vertical and Horizontal
		total[n] = _mm_add_epi16(sumh, sumv);
	}

	// Pack the two values
	total_packed = _mm_packus_epi16(total[0], total[1]);

	// Store
	_mm_storeu_si128(reinterpret_cast<__m128i*>(data_out + lay*size + i * width + j), total_packed);
}

void sse_sobel_edge_detection(unsigned char *data_out,
                              unsigned char *data_in, unsigned height,
                              unsigned width)
{
	unsigned i, j, lay;
	for (lay = 0; lay < 3; lay++)
		for (i = 1; i < height - 1; ++i)
		{
			// Compute firsts chunks
			for (j = 1; j < width - 17; j += 16)
				compute_sobel(data_out, data_in, height, width, lay, i, j);

			// Compute last chunk as 16u long
			compute_sobel(data_out, data_in, height, width, lay, i, width - 17);
		}
}


void basic_sobel_edge_detection(unsigned char *data_out,
                                unsigned char *data_in,
                                unsigned height, unsigned width)
{
    /* Sobel matrices for convolution */
    int sobelv[3][3] = { {-1, -2, -1}, {0, 0, 0}, {1, 2, 1} };
    int sobelh[3][3] = { {-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1} };
    unsigned int size, i, j, lay;
    size = height * width;

	for (lay = 0; lay < 3; lay++)
	{
        for (i = 1; i < height - 1; ++i)
		{
            for (j = 1; j < width - 1; j++)
			{
                int sumh = 0, sumv = 0;

                /* Convolution part */
                for (int k = -1; k < 2; k++)
                    for (int l = -1; l < 2; l++)
					{
                        sumh = sumh + sobelh[k + 1][l + 1] * (int) data_in[lay * size + (i + k) * width + (j + l)];
                        sumv = sumv + sobelv[k + 1][l + 1] * (int) data_in[lay * size + (i + k) * width + (j + l)];
                    }
				       
                data_out[lay * size + i * width + j] =    abs(sumh / 8) + abs(sumv / 8);
            }
        }
    }
}


int main(int argc, char **argv)
{

    /* Some variables */
    bmp_header header;
    unsigned char *data_in, *data_out;
    unsigned int size;
    int rep;

    test_func functions[2] =
        { basic_sobel_edge_detection, sse_sobel_edge_detection };
    if (argc != 4) {
	std::cout << "Usage " << argv[0];
	std::cout << " <InFile> <OutFile> <0:basic_sobel or 1 :sse_sobel";
	std::cout << std::endl << std::endl;
        exit(0);
    }

    bmp_read(argv[1], &header, &data_in);
    
    size = header.height * header.width;
    data_out = new unsigned char[3 * size];
	memcpy_s(data_out, 3 * size, data_in, 3*size);
    printf("Resolution: (%d,%d) -> Size: %d\n", header.height,
           header.width, size);

    prof_time_t start = 0, end = 0;
    start_measure(start);
    end_measure(end);

    int which_func = std::atoi(argv[3]);
    prof_time_t base_time = end - start;
    start_measure(start);
    for (rep = 0; rep < REP; rep++) {
        functions[which_func] (data_out, data_in, header.height,
                               header.width);
    }
    end_measure(end);

    prof_time_t cycles_taken = end - start - base_time;

    std::cout << "Cycles taken: " << cycles_taken / REP << std::endl;


    bmp_write(argv[2], &header, data_out);

    return (0);
}
