; Name:       Square Root Calculation with Parameters
; Author:     gabriel.m
; Class:      cs315
; Assignment: 1

section .data
    format: db 'Value=%7d Sqroot=%4d', 10, 0


section .text
global _sq_root_compute_array
global _sq_root_compute_varargs
extern _printf


_sq_root_compute_array:             ; array function
    push ebp                        ; store previous stack
    mov ebp, esp                    ; start new stack chunk

    mov ecx, 0                      ; set numcounter to zero
    for_each_num:                   ; loop each number
        mov edx, [ebp+12]			; get head of the array
        mov edx, [edx+4*ecx]        ; access current number
        mov eax, 1                  ; set sqroot value to one
        push ecx                    ; save ecx counter
        mov ecx, 1                  ; initialize the first odd number
        mov ebx, edx                ; initialize with current value

        compute_square:
            sub ebx, ecx            ; subtract the odd number to the value
            cmp ebx, 0              ; if zero
            je finished_square      ; then we finished
            add ecx, 2              ; else we increase the odd value
            inc eax                 ; and we increment the sq value
            jmp compute_square      ; compute the next value
            finished_square:        ; square value is eax
            pop ecx                 ; restore ecx counter

        push ecx                    ; save counter
        push eax                    ; pass sqroot (param 3)
        push edx                    ; pass number (param 2)
        push format                 ; pass format (param 1)

        call _printf                ; call printf

        add esp, 4                  ; free format (param 1)
        add esp, 4                  ; free number (param 2)
        add esp, 4                  ; free number (param 3)
        pop ecx                     ; restore counter
        inc ecx                     ; counter++

        cmp ecx, [ebp+8]            ; test loop condition
        jne for_each_num            ; execute loop condition

    mov esp, ebp                    ; free current stack
    pop ebp                         ; restore previous stack
    ret

	
_sq_root_compute_varargs:           ; variadic function
	push ebp                        ; store previous stack
    mov ebp, esp                    ; start new stack chunk

    mov ecx, 0                      ; set numcounter to zero
    for_each_num_:                  ; loop each number
        lea edx, [ebp+12]			; get head of the array
        mov edx, [edx+4*ecx]        ; access current number
        mov eax, 1                  ; set sqroot value to one
        push ecx                    ; save ecx counter
        mov ecx, 1                  ; initialize the first odd number
        mov ebx, edx                ; initialize with current value

        compute_square_:
            sub ebx, ecx            ; subtract the odd number to the value
            cmp ebx, 0              ; if zero
            je finished_square_     ; then we finished
            add ecx, 2              ; else we increase the odd value
            inc eax                 ; and we increment the sq value
            jmp compute_square_     ; compute the next value
            finished_square_:       ; square value is eax
            pop ecx                 ; restore ecx counter

        push ecx                    ; save counter
        push eax                    ; pass sqroot (param 3)
        push edx                    ; pass number (param 2)
        push format                 ; pass format (param 1)

        call _printf                ; call printf

        add esp, 4                  ; free format (param 1)
        add esp, 4                  ; free number (param 2)
        add esp, 4                  ; free number (param 3)
        pop ecx                     ; restore counter
        inc ecx                     ; counter++

        cmp ecx, [ebp+8]            ; test loop condition
        jne for_each_num_           ; execute loop condition

    mov esp, ebp                    ; free current stack
    pop ebp                         ; restore previous stack
    ret