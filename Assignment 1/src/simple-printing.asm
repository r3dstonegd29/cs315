; Name:       Simple Printing
; Author:     gabriel.m
; Class:      cs315
; Assignment: 1

section .data
    format: db 'Count=%3d Element=%5d', 10, 0
    numbers: dd 4, 26, 33, 42, 64, 79, 93, 876, 1011, 2232, 5707, 9802

section .text
global _simple_printing
extern _printf

_simple_printing:                  ; main function
    mov ecx, 0                     ; set counter to zero

    for_each_num:                  ; loop start
        mov edx, [numbers + ecx*4] ; access current number

        push edx                   ;  pass number (param 3)
        push ecx                   ;  pass counter (param 2)
        push format                ; pass format (param 1)

        call _printf               ; call printf

        add esp, 4                 ; free format (param 1)
        pop ecx                    ; restore counter (param 2)
        add esp, 4                 ; free number (param 3)

        inc ecx                    ; counter++

        cmp ecx, 12                ; test loop condition
        jne for_each_num           ; execute loop condition

    ret