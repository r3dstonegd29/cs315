#include <stdio.h>
#include <stdlib.h>
#include <iostream>

extern "C" void __cdecl simple_printing();
extern "C" void __cdecl sqroot_estimation();
extern "C" void __cdecl sq_root_compute_array(int num_of_elements, unsigned int * elements);
extern "C" void __cdecl sq_root_compute_varargs(int num_of_elements, ...);

int main( void )
{
    std::cout << "Printing:" << std::endl;
    simple_printing();
    std::cout << std::endl;

    std::cout << "Square-root using locals:" << std::endl;
    sqroot_estimation();
    std::cout << std::endl;

    std::cout << "Square-root using parameters:" << std::endl;
    unsigned int elements[]={25, 169, 9801, 28224, 136161};
    sq_root_compute_array(5, elements);
    std::cout << std::endl;

    std::cout << "Square-root using variadic:" << std::endl;
    sq_root_compute_varargs(5, 25, 169, 9801, 28224, 136161);
    std::cout << std::endl;

    return 0;
}